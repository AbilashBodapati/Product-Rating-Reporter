import java.util.Scanner;

public class ProductQuantityTest {
    public static void main(String[] args) {

        // Create a new Product Object to test Price functions
        Product prod = new Product();

        // Testing setting and getting quantity
        System.out.print("Enter a Quantity (to end enter -1): ");
        Scanner inQuantity = new Scanner(System.in);
        int inputQuantity = inQuantity.nextInt();

        while (inputQuantity != -1) {
            prod.setQuantity(inputQuantity);
            int quantity = prod.getQuantity();

            System.out.println(quantity);
            System.out.println("");

            System.out.print("Enter a Quantity (to end enter -1): ");
            inQuantity = new Scanner(System.in);
            inputQuantity = inQuantity.nextInt();

        }

        inQuantity.close();
    }
}