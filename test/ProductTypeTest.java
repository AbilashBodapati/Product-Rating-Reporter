import java.util.Scanner;

public class ProductTypeTest {
    public static void main(String[] args) {

        // Create a new Product Object to test Price functions
        Product prod = new Product();

        // Testing setting and getting type
        System.out.print("Enter a Type (to end enter x): ");
        Scanner inType = new Scanner(System.in);
        String inputType = inType.nextLine();

        while (!inputType.equals("x")) {
            prod.setType(inputType);
            String type = prod.getType();

            System.out.println(type);
            System.out.println("");

            System.out.print("Enter a Type (to end enter x): ");
            inType = new Scanner(System.in);
            inputType = inType.nextLine();

        }

        System.out.println("End of the Test");

        inType.close();
    }
}