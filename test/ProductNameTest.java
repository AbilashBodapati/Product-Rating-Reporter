import java.util.Scanner;

/**
 * Test code for Product Class
 *
 * @author Abilash Bodapati
 * @version 20190201
 */

public class ProductNameTest {

    public static void main(String[] args) {

        Product prod = new Product();

        // Testing setting and getting name

        System.out.print("Enter a name (to end enter x): ");
        Scanner inName = new Scanner(System.in);
        String inputName = inName.nextLine();

        while (!inputName.equals("x")) {
            prod.setName(inputName);
            String name = prod.getName();

            System.out.println(name);
            System.out.println("");

            System.out.print("Enter a name (to end enter x): ");
            inName = new Scanner(System.in);
            inputName = inName.nextLine();

        }

        // File i/o stream close statements.
        inName.close();

    }

}