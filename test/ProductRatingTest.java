import java.util.Scanner;

public class ProductRatingTest {
    public static void main(String[] args) {

        Product prod = new Product();

        // Testing setting and getting rating, rating count, and the average rating
        System.out.print("Enter a Type (to end enter -1): ");
        Scanner inRating = new Scanner(System.in);
        int inputRating = inRating.nextInt();

        while (inputRating != -1) {
            prod.addUserRating(inputRating);

            System.out.print("Enter a Type (to end enter -1): ");
            inRating = new Scanner(System.in);
            inputRating = inRating.nextInt();

        }

        for (int i = 0; i < prod.getUserRatingCount(); i++) {
            int rating = prod.getUserRating(i);
            System.out.println("Rating number " + i + " : " + rating);
            System.out.println(
                    "There are " + prod.getUserRatingCount() + " ratings.");
        }

        System.out.println("The average rating is " + prod.getAvgUserRating());

        // Close I/O Stream
        inRating.close();
    }
}