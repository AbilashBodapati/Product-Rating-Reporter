import java.util.Scanner;

public class ProductPriceTest {

    public static void main(String[] args) {

        // Create a new Product Object to test Price functions
        Product prod = new Product();

        // Testing setting and getting price.
        System.out.print("Enter a Price (to end enter -1): ");
        Scanner inPrice = new Scanner(System.in);
        double inputPrice = inPrice.nextDouble();

        // User input
        while (inputPrice != -1) {
            prod.setPrice(inputPrice);
            double price = prod.getPrice();

            System.out.println(price);
            System.out.println("");

            System.out.print("Enter a Price (to end enter -1): ");
            inPrice = new Scanner(System.in);
            inputPrice = inPrice.nextDouble();

        }

        System.out.println("End of the Test");

        inPrice.close();
    }
}