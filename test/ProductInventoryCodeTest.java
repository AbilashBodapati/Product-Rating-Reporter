import java.util.Scanner;

/**
 * Test code for Product Class
 *
 * @author Abilash Bodapati
 * @version 20190201
 */

public class ProductInventoryCodeTest {

    public static void main(String[] args) {

        Product prod = new Product();

        // Testing setting and getting Inventory Code
        System.out.print("Enter an Inventory Code (to end enter x): ");
        Scanner inCode = new Scanner(System.in);
        String inputCode = inCode.nextLine();

        while (inputCode.equals("")) {
            prod.setInventoryCode(inputCode);
            String inventoryCode = prod.getInventoryCode();
            System.out.println(inventoryCode);
            System.out.println("");

            System.out.print("Enter an Inventory Code (to end enter x): ");
            inCode = new Scanner(System.in);
            inputCode = inCode.nextLine();

        }

        inCode.close();

    }

}