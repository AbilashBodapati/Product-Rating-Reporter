import java.io.File;
import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 * This Program gives a detailed report of products from a file.
 *
 * @author Abilash Bodapati
 * @version 20190201
 *
 */

public class OOPReport {

    /**
     * Void method to do the heading of the inventory
     */
    public static void header() {

        // This block of code gives the inventory report a clean heading
        System.out.println("");
        System.out.println("Product Inventory Summary Report");
        System.out.println("");
        System.out.println(
                "------------------------------------------------------------------------------");
        System.out.println(
                "Product Name                      I Code    Type   Rating # Rat. Quant. Price");
        System.out.println(
                "--------------------------------- --------- -----  ------ ------ ------ ------");

    }

    public static void fileParser(Scanner fileReader,
            ArrayList<Product> productLine) {

        // Iterator to help keep track of where the reader
        //  is while scanning the file.
        int i = 1;

        // Create a new Product object
        Product prod = new Product();

        // Loop through the file given and sort the data using basic
        // inference of the input.
        while (fileReader.hasNext()) {

            // Get the string from the file to check its data type
            String productEntry = fileReader.nextLine();

            // Using if-else if-else control flow to direct the data into its
            // respective data type of the class Product.
            if (i == 1) {
                // We can assume the entry is the item "NAME"

                // This block of code adds spaces after the name for clean
                // display of the inventory
                int nameLength = productEntry.length();
                int numOfSpaces = 31 - nameLength;

                for (int j = 0; j < numOfSpaces; j++) {
                    productEntry += " ";
                }

                // Setting the name of the product
                prod.setName(productEntry);

            } else if (i == 2) {
                // We can assume the entry is the "ITEM CODE"

                // Setting the Inventory Code value of the product
                prod.setInventoryCode(productEntry);

            } else if (i == 3) {
                // We can assume the entry is the "QUANTITY"

                // Setting the quantity of the Product
                prod.setQuantity(Integer.parseInt(productEntry));

            } else if (i == 4) {
                // We can assume the entry is the "PRICE"

                // Setting the price of the unit Product
                prod.setPrice(Double.parseDouble(productEntry));

            } else if (i == 5) {
                // We can assume the entry is "TYPE"

                // This block of code adds spaces after the name for clean
                // display of the inventory
                int nameLength = productEntry.length();
                int numOfSpaces = 4 - nameLength;

                for (int j = 0; j < numOfSpaces; j++) {
                    productEntry += " ";
                }

                // Setting type of the product
                prod.setType(productEntry);

            } else if (i >= 6) {
                // We can assume the entry is "Ratings".

                // We check if the product has any Ratings or not using -1 string
                if (!productEntry.equals("-1")) {

                    // If Rating Exists we add the rating in the product's rating list
                    prod.addUserRating(Integer.parseInt(productEntry));

                } else {

                    // Reset the Parameters to get the details of the next product
                    i = 0;
                    productLine.add(prod);
                    prod = new Product();
                }

            }
            // Start Iteration for next Product
            i++;
        }

    }

    /**
     * This void method must print the array list of products in a clean form.
     *
     * @param product
     */
    public static void inventoryDetails(ArrayList<Product> product) {

        // Looping through the ArrayList of Products to work with each product
        // that is been set with it's respective values
        for (int i = 0; i < product.size(); i++) {

            // Retrieve one Product from the ArrayList
            Product prod = product.get(i);

            // String of Stars that will show the Avg Rating of the product
            String stars = "";
            for (int j = 0; j < prod.getAvgUserRating(); j++) {
                stars += "*";
            }

            // stars.length() == prod.getAvgUserRating
            int starsLength = stars.length();
            // Creating spaces for stars("RATING") to make the report look clean
            int numOfSpacesStars = 6 - starsLength;
            for (int k = 0; k < numOfSpacesStars; k++) {
                stars += " ";
            }

            // Creating spaces for "QUANTITY" to make the report look clean
            String quantity = Integer.toString(prod.getQuantity());
            int quantityLength = quantity.length();
            int numOfSpacesQuantity = 6 - quantityLength;
            for (int l = 0; l < numOfSpacesQuantity; l++) {
                quantity = " " + quantity;
            }

            // Creating spaces for "PRICE" to make the report look clean
            String price = Double.toString(prod.getPrice());
            int priceLength = price.length();
            int numOfSpacesPrice = 7 - priceLength;
            for (int k = 0; k < numOfSpacesPrice; k++) {
                price = " " + price;
            }

            // Printing line by line the details of the product
            System.out.println(prod.getName() + "   " + prod.getInventoryCode()
                    + "   " + prod.getType() + "   " + stars + "   "
                    + prod.getUserRatingCount() + "   " + quantity + price);
        }

    }

    /**
     * Retrieve the name of the lowest rating
     *
     * @param product
     * @param index
     * @return
     */
    private static String lowestRatingName(ArrayList<Product> product,
            int index) {

        // Get the Product corresponding to the index with the lowest rating
        Product lowestProductRating = product.get(index);

        // Convert the name into StringBuilder to get rid of the trailing spaces
        StringBuilder lowAmtRating = new StringBuilder(
                lowestProductRating.getName());

        // Create a string to return the name
        String name = "";

        // Reverse the string to remove the spaces in the end so we can avoid
        // changing the actual string
        lowAmtRating.reverse();
        while (lowAmtRating.charAt(0) == ' ') {
            lowAmtRating.deleteCharAt(0);
        }

        // Reverse the string and set it equal to the string to return
        lowAmtRating.reverse();
        name = lowAmtRating.toString();

        return name;
    }

    /**
     * Get the name and the rating (lowest)
     *
     * @param ratingList
     * @param ratingCountList
     * @param product
     * @return
     */
    private static String lowestRating(ArrayList<Integer> ratingList,
            ArrayList<Integer> ratingCountList, ArrayList<Product> product) {

        // Get the first rating from the list
        int low = ratingList.get(0);
        //Iterator to keep track of which product has the rating associated to
        int count = 0;

        // String that will be returned
        String value = "";

        // Loop logic to check every rating for the lowest rating
        int i = -1;
        for (int j = 1; j < ratingList.size(); j++) {
            int element = ratingList.get(j);

            // Condition if the next element is the list is less and the first occurence
            if (low > element) {
                low = element;
                count = ratingCountList.get(j);
                i = j;
            } else if (low == element) {

                // A condition if the smallest has a greater countlist
                if (count < ratingCountList.get(j)) {
                    count = ratingCountList.get(j);
                    i = j;
                }
            }
        }

        // Create a string that will show the avg rating using the stars
        String rate = "";
        for (int z = 0; z < low; z++) {
            rate += "*";
        }

        // Method call to get the name of the product associated with its
        // position in the list
        String name = lowestRatingName(product, i);

        // Concatenate the name and its avg rate
        value = name + " (" + rate + ")";

        return value;
    }

    /**
     * Retrieve the name of the highest rating.
     *
     * @param product
     * @param index
     * @return
     */
    private static String highestRatingName(ArrayList<Product> product,
            int index) {

        // Get the Product corresponding to the index with the highest rating
        Product highestProductRating = product.get(index);

        // Convert the name into StringBuilder to get rid of the trailing spaces
        StringBuilder highAmtRating = new StringBuilder(
                highestProductRating.getName());

        // Create a string to return the name
        String name = "";

        // Reverse the string to remove the spaces in the end so we can avoid
        // changing the actual string
        highAmtRating.reverse();
        while (highAmtRating.charAt(0) == ' ') {
            highAmtRating.deleteCharAt(0);
        }

        // Reverse the string and set it equal to the string to return
        highAmtRating.reverse();
        name = highAmtRating.toString();

        return name;

    }

    /**
     * Get the name and rating (highest)
     *
     * @param ratingList
     * @param ratingCountList
     * @param product
     * @return
     */
    private static String highestRating(ArrayList<Integer> ratingList,
            ArrayList<Integer> ratingCountList, ArrayList<Product> product) {

        // Get the first rating from the list
        int high = 0;
        //Iterator to keep track of which product has the rating associated to
        int count = 0;

        // String that will be returned
        String value = "";

        // Loop logic to check every rating for the lowest rating
        int i = -1;
        for (int j = 0; j < ratingList.size(); j++) {
            int element = ratingList.get(j);
            // Condition if the next element is the list is less and the first occurence
            if (high < element) {
                high = element;
                count = ratingCountList.get(j);
                i = j;
            } else if (high == element) {
                // A condition if the smallest has a greater countlist
                if (count > ratingCountList.get(j)) {
                    count = ratingCountList.get(j);
                    i = j;
                }
            }
        }

        // Create a string to return the name
        String rate = "";
        for (int z = 0; z < high; z++) {
            rate += "*";
        }

        // Method call to get the name of the product associated with its
        // position in the list
        String name = highestRatingName(product, i);

        // Concatenate the name and its avg rate
        value = name + " (" + rate + ")";

        return value;
    }

    /**
     *
     * Method should give the name of the product that has the highest Dollar
     * Amt.
     *
     * @param prod
     * @param index
     * @return String - name associated with the highest total dollar Amt.
     */
    public static String highestAmtName(ArrayList<Product> prod, int index) {

        // Get the Product with the highest amount
        Product highestProductAmt = prod.get(index);

        // Get the name to get rid of the trailing spaces
        StringBuilder highAmtName = new StringBuilder(
                highestProductAmt.getName());

        // Create a string variable to return
        String name = "";

        // Reverse the string
        highAmtName.reverse();

        // Loop to remove the spaces
        while (highAmtName.charAt(0) == ' ') {
            highAmtName.deleteCharAt(0);
        }

        // Reverse the string
        highAmtName.reverse();

        // Convert Stringbuilder to string and return
        name = highAmtName.toString();

        return name;
    }

    /**
     * This method should return a string of the name and its highest dollar amt
     * concatenated together.
     *
     * @param totalDollarAmt
     * @param product
     * @return String - value
     */
    public static String highestAmt(ArrayList<Double> totalDollarAmt,
            ArrayList<Product> product) {

        // Get the first element of the product's price
        double high = 0;

        // Iterator value
        int i = -1;

        // Loop logic to check for the highest total amount
        for (int j = 0; j < totalDollarAmt.size(); j++) {
            double element = totalDollarAmt.get(j);
            if (high < element) {
                high = element;
                i = j;
            }
        }

        // Number formatting into currency
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String highAmt = format.format(high);

        // Get the name associated to highest amount
        String name = highestAmtName(product, i);

        // Concatenate the name and the total highest amount and return it
        String value = name + " (" + highAmt + ")";

        return value;
    }

    /**
     * Method should give the name of the product that has the lowest Dollar
     * Amt.
     *
     * @param prod
     * @param index
     * @return String - name associated with the lowest total dollar Amt.
     */
    public static String lowestAmtName(ArrayList<Product> prod, int index) {

        // Get the Product with the lowest amount
        Product lowestProductAmt = prod.get(index);

        // Get the name to get rid of the trailing spaces
        StringBuilder lowAmtName = new StringBuilder(
                lowestProductAmt.getName());

        // Create a string variable to return
        String name = "";

        // Reverse the string
        lowAmtName.reverse();

        // Loop to remove the spaces
        while (lowAmtName.charAt(0) == ' ') {
            lowAmtName.deleteCharAt(0);
        }

        // Reverse the string
        lowAmtName.reverse();

        // Convert Stringbuilder to string and return
        name = lowAmtName.toString();

        return name;
    }

    /**
     * This method should return a string of the name and its lowest dollar amt
     * concatenated together.
     *
     * @param totalDollarAmt
     * @param product
     * @return String - value
     */
    public static String lowestAmt(ArrayList<Double> totalDollarAmt,
            ArrayList<Product> product) {

        // Get the first element of the product's price
        double low = totalDollarAmt.get(0);

        // Iterator value
        int i = -1;

        // Loop logic to check for the lowest total amount
        for (int j = 1; j < totalDollarAmt.size(); j++) {
            double element = totalDollarAmt.get(j);
            if (low > element) {
                low = element;
                i = j;
            }
        }

        // Number formatting into currency
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String lowAmt = format.format(low);

        // Get the name associated to lowest amount
        String name = lowestAmtName(product, i);

        // Concatenate the name and the total lowest amount and return it
        String value = name + " (" + lowAmt + ")";

        return value;
    }

    /**
     *
     * Void inventory to do the End-line and further detail
     */
    public static void endDetails(ArrayList<Product> product) {

        // Line that end the product details and starts
        System.out.println(
                "------------------------------------------------------------------------------");
        // Total number of products in the inventory
        System.out.println("Total products in database: " + product.size());

        // Create ArrayLists of Avg rating and the number of ratings per product
        ArrayList<Integer> ratingList = new ArrayList<>();
        ArrayList<Integer> ratingCountList = new ArrayList<>();

        // Loop to add the avg ratings and the number counts of each product
        // in its respective ArrayList
        for (int i = 0; i < product.size(); i++) {
            Product prod = product.get(i);
            ratingList.add(prod.getAvgUserRating());
            ratingCountList.add(prod.getUserRatingCount());
        }

        // String Methods to get the name and the rating for the highest and lowest
        String highRate = highestRating(ratingList, ratingCountList, product);
        String lowRate = lowestRating(ratingList, ratingCountList, product);

        // Print the strings
        System.out.println("Highest Average Ranked item: " + highRate);
        System.out.println("Lowest Average Ranked item: " + lowRate);

        // Create the ArrayList of doubles that consist of the price
        ArrayList<Double> totalDollarAmt = new ArrayList<>();

        // Add the total amount of the price of each product in the ArrayList
        for (int i = 0; i < product.size(); i++) {
            Product prod = product.get(i);
            double totalAmt = prod.getQuantity() * prod.getPrice();
            totalDollarAmt.add(totalAmt);
        }

        // String method for the highest amount and the lowest amount
        String highAmt = highestAmt(totalDollarAmt, product);
        String lowAmt = lowestAmt(totalDollarAmt, product);

        // Print the amounts to console
        System.out.println("Highest Total Dollar item: " + highAmt);
        System.out.println("Lowest Total Dollar item: " + lowAmt);

        // End line of the inventory
        System.out.println(
                "------------------------------------------------------------------------------");

    }

    /**
     * Main Method
     *
     * @param args
     */
    public static void main(String[] args) {

        // Ask User for the file name to read.
        System.out.print("Enter an inventory filename: ");
        Scanner inputFile = new Scanner(System.in);
        String name = inputFile.nextLine();
        File file = new File(name);

        // Initialize scanner
        Scanner fileReader;

        // Add try-catch for i/o handling
        try {

            // New Constructor to scan the file.
            fileReader = new Scanner(file);

            // Method call to print the headings of the inventory
            header();

            // Create an array list of type product
            ArrayList<Product> productLine = new ArrayList<>();

            // Method Call to parse the file into an Array List of products
            fileParser(fileReader, productLine);

            // Method call for the product inventory details
            inventoryDetails(productLine);

            // Method call to get the remaining information about the series of products
            endDetails(productLine);

            // Close i/o stream
            fileReader.close();

        } catch (FileNotFoundException e) {

            // An error message if client ever enters an invalid file name that does not exist
            System.err.println("File Not Found!!");
        }

        // Close i/o stream
        inputFile.close();
    }
}